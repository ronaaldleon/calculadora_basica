var panel = document.getElementById("panel");

var inverso = 0, cuadrado, raiz = 0, raiz2 = 0;
document.getElementById('panel').style.backgroundColor = '#00C785';
function unidades(n) {
	panel.innerHTML += n;
}

function igual() {
	/*eval() Evalúa un código JavaScript representado como una cadena de caracteres
	sin distincion de tipo de dato, en este caso evalua una expresion y la resuelbe*/
	c = eval(panel.innerHTML);
	panel.innerHTML = c;
}

function clc() {
	panel.innerHTML = '';
}

/*NUMEROS*/
document.getElementById("0").addEventListener("click", function () {
	unidades(0);
});
document.getElementById("1").addEventListener("click", function () {
	unidades(1);
});
document.getElementById("2").addEventListener("click", function () {
	unidades(2);
});
document.getElementById("3").addEventListener("click", function () {
	unidades(3);
});
document.getElementById("4").addEventListener("click", function () {
	unidades(4);
});
document.getElementById("5").addEventListener("click", function () {
	unidades(5);
});
document.getElementById("6").addEventListener("click", function () {
	unidades(6);
});
document.getElementById("7").addEventListener("click", function () {
	unidades(7);
});
document.getElementById("8").addEventListener("click", function () {
	unidades(8);
});
document.getElementById("9").addEventListener("click", function () {
	unidades(9);
});
/*FIN NUMEROS*/


document.getElementById("punto").addEventListener("click", function () {
	unidades('.');
});


/*OPERACIONES*/
document.getElementById("suma").addEventListener("click", function () {
	unidades('+');
});
document.getElementById("resta").addEventListener("click", function () {
	unidades('-');
});
document.getElementById("multi").addEventListener("click", function () {
	unidades('*');
});
document.getElementById("division").addEventListener("click", function () {
	unidades('/');
});
document.getElementById("residuo").addEventListener("click", function () {
	unidades('%');
});
document.getElementById("inverso").addEventListener("click", function () {
	inverso = Math.pow(panel.innerHTML, -1);
	panel.innerHTML = inverso;
});
document.getElementById("cuadrado").addEventListener("click", function () {
	cuadrado = Math.pow(panel.innerHTML, 2);
	panel.innerHTML = cuadrado;
});

document.getElementById("raiz").addEventListener("click", function () {
	raiz = eval(panel.innerHTML);
	if (raiz < 0) {
		panel.innerHTML = "Math Error";
		/*location.href = "../html/calculadora.html";*/
	} else {
		raiz2 = Math.sqrt(raiz);
		panel.innerHTML = raiz2;
	}

});

document.getElementById("clear").addEventListener("click", function () {
	clc();
});

document.getElementById("igual").addEventListener("click", function () {
	var campo = eval(panel.innerHTML);
	if(campo == null){
		panel.innerHTML = '';
	}else if(isNaN(campo)){
		panel.innerHTML = "Math Error";
	}else{
		igual();
	}
});
/*FIN OPERACIONESS*/